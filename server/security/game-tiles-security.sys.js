/**
 * Create, Read, Update, Delete server side permissions for the game-tiles collection.
 * @copyright ByteCubed 2016
 * @author Justin Williams
 * @module server/init/game-tiles-security
 * 
 * @rationale
 * Separating out the security so that it is only visible even in minified format on the server is a good way to prevent others easily seeing holes in your security logic.
 * This is often the next file one creates after making the shared Mongo collection file in the /collections/ path.
 * It is required for the client to work at all once meteor insecure and autopublish are both removed.
 * The .allow and .deny methods provide the security hooks needed for editing data such as creat (insert), update, and delete (remove).
 * The .publish method provides filtering security around read operations.
 * This is a barebones example that's not very good security as it allows any logged in user to do any collection modification.
 * It would be smart to lock down what fields they can add/modify and perhaps deny some operations based on verified email addresses, etc.
 * The name of this file includes .sys. This is how tsumina:meteor-systemjs finds the file server side so we can use the same import/export syntax for both.
 * It is unnecessary to name js files on the client with the .sys. convention. This will change in future releases.
 */

import {GameTiles} from 'collections/game-tiles';

GameTiles.allow({
    insert: function() {
        var user = Meteor.user();
        return !!user;
    },
    update: function() {
        var user = Meteor.user();
        return !!user;
    },
    remove: function() {
        var user = Meteor.user();
        return !!user;
    }
});

Meteor.publish(GameTiles.NAME, function() {
    return GameTiles.find();
});

