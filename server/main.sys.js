/**
 * Entry point for the server side code typically focused on setup and configuration.
 * @copyright ByteCubed 2016
 * @author Justin Williams
 * @module server/main
 * 
 * @rationale
 * This is the entry point file for the server side, and the sister file to the client's app.js.
 * The startup method is used to ensure that any code within it is executed first, prior to anything else, regardless of build order, and is good for setting environment variables or other configurations.
 * Note the lack of password hard coded in this file. The "sendVerificationEmail" parameter is commented out on servers which aren't explicitly whitelisted by IP in gmail's administration panel.
 * Because we're using system.js, files are only going to be included if they are imported from main. 
 * For this reason we exported a method to initialize all the data in the app for fresh instances and run that check just in case with initData().
 * We import security files without reference variables so they simply execute directly. This is an alternate approach to the data initialization seen earlier.
 * The server side tends to look much lighter, at least at first glance, since most business logic like validation occurs in the security files for collections which are defined outside of the /server/ directory so they are shared.
 * Then there's the configurations which are either added here directly or imported and referenced from this file.
 * And it's usually fine to put all data initialization together in /server/init/load-from-new.
 * Meteor and Node making it all look easy compared to pretty much any other stack which tends to be very server heavy.
 * The name of this file includes .sys. This is how tsumina:meteor-systemjs finds the file server side so we can use the same import/export syntax for both.
 * It is unnecessary to name js files on the client with the .sys. convention. This will change in future releases.
 */

import {initData} from 'server/init/load-from-new';
import 'server/security/game-tiles-security';

Meteor.startup( function() {
    process.env.MAIL_URL = "smtp://smtp-relay.gmail.com:465";
    Accounts.emailTemplates.from="justin@williams.ink";
    Accounts.emailTemplates.siteName="williams.ink";
    Accounts.emailTemplates.verifyEmail.subject=function(user) {
        return "Email Registration Confirmation for Tutorial";
    };
    Accounts.emailTemplates.verifyEmail.text = function(user, url) {

    return 'Thank you for registering! Please click on the following link to verify your email address to make your changes public: \r\n' + url;
    };
    //Accounts.config({sendVerificationEmail: true});
});

initData();
