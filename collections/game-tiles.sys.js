/**
 * The Mongo and MongoMini shared definition of the gameTiles collection.
 * @copyright ByteCubed 2016
 * @author Justin Williams
 * @module collections/game-tiles
 * 
 * @rationale
 * Taking advantage of the polymorphic nature of client/server/database, this simple file ensures all three are using exactly the same thing for this data collection.
 * First, in case it's useful elsewhere, we define a constant for the name of the collection which we will then attach directly to the exported variable.
 * The scope for the GameTiles variable is broad as it is exported. As such, var seemed more appropriate to me than let.
 * The name of this file includes .sys. This is how tsumina:meteor-systemjs finds the file server side so we can use the same import/export syntax for both.
 * It is unnecessary to name js files on the client with the .sys. convention. This will change in future releases.
 */

const _name = 'gameTiles';

export var GameTiles = new Mongo.Collection(_name);    

GameTiles.NAME = _name;

// Optionall define schema here.