/**
 * Deck as in slideshow type container for Card controls. Use Cards as contents in templates.
 * @copyright ByteCubed 2016
 * @author Justin Williams
 * @module client/containers/card-deck
 * 
 * @rationale
 * We need a way to define a set of views, possibly dynamically, which we can move between (show and hide) through a transition.
 * For now we've hard coded a fade transition in, but that could be a selection from a possible list or even custom defined.
 * Named deck like a presentation deck, and cards because cards go in a deck. Open to renaming when someone has a great idea!
 * Has a ready attribute to help prevent views from showing and hiding before they're all set up.
 * Suppress animation is helpful for things like routing where you don't want the app to delay at all in achieving a certain state.
 */

import {Component, View, Input, Output, EventEmitter, AfterContentInit, ContentChildren, QueryList} from 'angular2/core';
import {Card} from 'client/controls/card';

@Component({
    selector: 'card-deck',
    inputs: ['selectedCardIndex', 'suppressAnimation'],
    outputs: ['selectedCardIndexChange']
    })
@View({
    styles: [`
        .card-deck-defaults {
            width: 100%;
            height: 100%;
        }
    `],
    template: `
        <div class="card-deck-defaults" [hidden]="! ready">
            <ng-content></ng-content>
        </div>    
    `
    })
export class CardDeck {

    get selectedCardIndex() {
        return this._selectedCardIndex;
    }
    set selectedCardIndex(value) {
        if (this.views) {
            // Swap with default transition: currently hard coded fade.
            // Note, we're not yet handling views coming or going since the initialization.
            let initialCards = this.views.toArray();
            let oldCardIndex = this._selectedCardIndex;
            
            if (this.suppressAnimation) {
                initialCards[oldCardIndex].visible = false;
                initialCards[value].visible = true;
            } else {
                initialCards[oldCardIndex].transforms = {
                    opacity: 0,
                    fromOpacity: 1,
                    duration: 850,
                    easing: "easeOut",
                    allDone: () => { initialCards[oldCardIndex].visible = false; }
                };
                initialCards[value].transforms = {
                    opacity: 1,
                    fromOpacity: 0,
                    duration: 850,
                    easing: "easeOut"
                };
                initialCards[value].visible = true;
            }
            
        }
        this._selectedCardIndex = value;
        this.selectedCardIndexChange.next(value);
    }
    
    @ContentChildren(Card) set views(value) {
        this._views = value;
        this.selectedCardIndex = this._selectedCardIndex;
    }
    get views() {
        return this._views;
    }

    constructor() {
        this.suppressAnimation = false;
        this.ready = false;
        this._selectedCardIndex = 0;
        this._views = null;
        this.selectedCardIndexChange = new EventEmitter();
    }
    
    // Life Cycle Event Handling
    ngAfterContentInit() {
        globalUtils.tryNonRecursiveUpdates(() => {
            // Component content has been initialized
            let initialCards = this.views.toArray();
            
            // Initialize all the initial visibilities so only the current view shows.
            // Technically views could change but we're not handling that yet either here nor in the selected change event.
            for (let i = 0; i < initialCards.length; i++) {
                if (i !== this.selectedCardIndex) {
                    initialCards[i].visible = false;
                }
            }
            this.ready = true;
        });
    }
}