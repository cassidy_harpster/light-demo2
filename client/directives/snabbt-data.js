/**
 * Provides declarative access to binding Snabbt animation json to an element.
 * @copyright ByteCubed 2016
 * @author Justin Williams
 * @module client/directives/snabbt-data
 * 
 * @rationale
 * Javascript often out performs css with hardward accelerated transforms or opacity, however css takes better advantage of threading.
 * The best results can are often a mix of hw accelerated css + javascript.
 * Snabbt provides an extremely robust library to determine how to achieve high performance animation.
 * This directive is a good start but could use detection of arrays of json objects to perform chainging animation calls, etc. (Which Snabbt's library supports.)
 * Snabbt's default easings were limited so we use a modified version of the library that adds famo.us's easing transforms (all of them from easings.net).
 */

import {Directive, ElementRef} from 'angular2/core';

@Directive({
  selector: '[snabbtData]',
  providers: [ElementRef],
  inputs: ['snabbtData']
})
export class SnabbtDirective {

    static get parameters() {
        return [[ElementRef]];  
    }      
    
    // TODO: Support array of configs instead of just one and use snabbt chaining.
    get snabbtData() {
        return this._appAnim;
    }
    set snabbtData(value) {
        if(value) {
            window.snabbt(this._element.nativeElement, value);
        }
        this._snabbtData = value;
    }

    constructor(_el) {
        this._snabbtData = null;
        this._element = _el;
    }
}
