/**
 * An implementation of the main Login View.
 * @copyright ByteCubed 2016
 * @author Justin Williams
 * @module client/views/login-view
 * 
 * @rationale
 * This is an application specific view wired up to provide login.
 * Because it is a specific view based on a particular wireframe or "comp"/"mockup", it not only provides a centered <accounts-ui> but a mode selector to pick your team color.
 * The team color (based on lightsabers per the VERY important tutorial powerpoint meme) is what color a tile will change to when clicked.
 * The goal of the game then is for multiple players (and bots?) to compete to turn the most vortex of cards into their team's color.
 * The game persists even while nobody is playing and one can log in later on to discover someone else has stolen all of their hard earned cards.
 */

import {Component, View, Input, EventEmitter} from 'angular2/core';
import {Card} from 'client/controls/card'
import {AccountsUI} from 'meteor-accounts-ui';
import {IconMenu} from 'client/controls/icon-menu';


@Component({
    selector: 'login-view',
    inputs: ['teamColorIndex'],
    outputs: ['teamColorIndexChange']
    })
@View({
    styles: [`
        .sign-on {
            position: absolute;
            top: 49%;
            left: 49%;
            transform: translate(-50%, -50%);
        }
        .mode-selector {
            position: absolute;
            top: 0px;
            right: 0px;
        }
    `],
    template: `
        <accounts-ui class="sign-on"></accounts-ui>
        <icon-menu  #modeMenu 
                    menuAlign="right" 
                    class="mode-selector" 
                    [menuItems]="teamColors"
                    [(selectedItem)]="teamColorIndex"></icon-menu>
        `,
    directives: [AccountsUI, IconMenu]
})
export class LoginView {
    
    get teamColorIndex() {
        return this._teamColorIndex;
    }
    set teamColorIndex(value) {
        if(value != this._teamColorIndex) {
            this._teamColorIndex = value;
            this.teamColorIndexChange.next(value);
        }
    }
    
    constructor() {
        this.transforms = null;
        this.teamColorIndexChange = new EventEmitter();
        this.teamColors = [
            {name: "Blue Team", value: "0"},
            {name: "Green Team", value: "1"},
            {name: "Red Team", value: "2"},
            {name: "Purple Team", value: "3"}
        ];
        this.teamColorIndex = 0;
    }
}