/**
 * Entry point for the client side of the application.
 * @copyright ByteCubed 2016
 * @author Justin Williams
 * @module client/app
 * 
 * @rationale
 * We'll use the entry point to layout all the high level views in an initial stack.
 * App will also provide common data used throughout most other views such as the User.
 * Finally, we'll use the App to hide our application loading until it is ready to go.
 * In the future this may include a loading animation if it gets too big.
 * Currently, you can't use @Inject in the constructor in ES6 so we define the constructor parameter types through the static 'parameters' getter.
 */

import {Component, View} from 'angular2/core';
import {bootstrap} from 'angular2-meteor';
import {FORM_DIRECTIVES, FORM_PROVIDERS} from 'angular2/common';

import {AuthenticatedUserService} from 'client/services/authenticated-user-service';

import {CardDeck} from 'client/containers/card-deck';
import {Card} from 'client/controls/card';
import {LoginView} from 'client/views/login-view';
import {VortexView} from 'client/views/vortex-view';


@Component({
    selector: 'app',
    providers: [AuthenticatedUserService]
})
@View({
    templateUrl: 'client/app.html',
    directives: [CardDeck, Card, LoginView, VortexView],
    styleUrls: [`app.next.css`]
})
class LightTutorial {
    static get parameters() {
        return [[AuthenticatedUserService]];  
    }
    
    constructor(_user) {
        this.dataReady = false;
        this.user = { 'loggedIn': false };
        globalUtils.tryNonRecursiveUpdates(() => {
            this.user = _user;
            this.dataReady = true;
            });
    }
}

bootstrap(LightTutorial, [FORM_DIRECTIVES, FORM_PROVIDERS]);